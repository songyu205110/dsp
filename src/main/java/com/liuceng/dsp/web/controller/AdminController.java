package com.liuceng.dsp.web.controller;

import com.liuceng.dsp.pojo.domain.Admin;
import com.liuceng.dsp.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

/**
 * Created by songyu on 2017/02/13.
 */
@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AdminService adminService;

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String getAll(ModelMap model) {
        List<Admin> admins = adminService.getAll();
        model.addAttribute("admin",admins);
        return "admin/list";
    }

}
