package com.liuceng.dsp.service;

import com.liuceng.dsp.mapper.AdminMapper;
import com.liuceng.dsp.pojo.domain.Admin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by songyu on 2017/02/13.
 */
@Service
public class AdminService {

    @Autowired
    AdminMapper adminMapper;

    public List<Admin> getAll() {
        List<Admin> admins = adminMapper.search();
        return admins;
    }

    public Admin getAdmin(String name){
        return adminMapper.selectByPrimaryKey(name);
    };
}
