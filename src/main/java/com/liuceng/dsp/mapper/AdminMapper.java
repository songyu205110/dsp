package com.liuceng.dsp.mapper;

import com.liuceng.dsp.common.annotations.MyBatisRepository;
import com.liuceng.dsp.pojo.domain.Admin;

import java.util.List;

@MyBatisRepository
public interface AdminMapper {
  int deleteByPrimaryKey(Integer id);

  int insert(Admin record);

  int insertSelective(Admin record);

  Admin selectByPrimaryKey(String name);

  int updateByPrimaryKeySelective(Admin record);

  int updateByPrimaryKey(Admin record);

  List<Admin> search ();
}
