package com.liuceng.dsp.common.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * MyBatisRepository.java 标识MyBatis的mapper,方便
 * {@link org.mybatis.spring.mapper.MapperScannerConfigurer} 的扫描。
 * 
 * @author
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface MyBatisRepository {

}
